<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mon Aquarium Connecté</title>
    <link rel="shortcut icon" type="image/png" href="image/favicon.ico"/>
    <link rel="stylesheet" href="css/ac.css">
  </head>
  <body>

    <!--Image haut de page -->
    <div class="img">
      <img src="image/affiche.png" alt="img" width="100%">
      <nav>
        <ul id="menu_horizontal">
          <img src="image/logo.jpg" class="logo" height="100%"></img>
          <li><a href="index2.php">Aquarium</a></li>
          <li><a href="contact.php">Contact</a></li>
          <li><a href="historique.html">Historique</a></li>
          <li><a href="login.html">Se deconnecter</a></li>
        </ul>
      </nav>
    </div>

    <div class="grid-container">

  <section>
         <FONT size="16"><strong><u> HISTORIQUE </u></strong></FONT>
        <br>
        <br>
        <br>
      <div id="alig2">
    <article>  
    <table>
    <thead> <!-- En-tête du tableau -->
   <tr>
       <td> <strong>&nbsp; Date &nbsp;</strong></td>
       <td> <strong>Température (°C)</strong> </td>
       <td> <strong>&nbsp; PH &nbsp;</strong> </td>
       <td> <strong>Niveau d'eau</strong> </td>
   </tr>
   </thead>
   <tr>
       <td>&nbsp; 29/11/2019 &nbsp;</td>
       <td> 19°C </td>
       <td> 7 </td>
       <td> Plein </td>
   </tr>
    <tr>
        <td> 30/11/2019 </td>
       <td> 20°C </td>
       <td> 7 </td>
       <td> Plein </td>
   </tr>
   <tr>
        <td> 1/12/2019 </td>
       <td> 21°C </td>
       <td> 6 </td>
       <td> Plein </td>
   </tr>
   <tr>
        <td> 2/12/2019 </td>
       <td> 21°C </td>
       <td> 6 </td>
       <td> Plein </td>
   </tr> 
   <tr>
        <td> 3/12/2019 </td>
       <td>  </td>
       <td>  </td>
       <td>  </td>
   </tr> 
</table>
</article>
<br>
<br>
<br>
          <article>  
    <table>
    <thead> <!-- En-tête du tableau -->
   <tr>
       <td> <strong>&nbsp; Date &nbsp;</strong></td>
       <td> <strong>Température (°C)</strong> </td>
       <td> <strong>&nbsp; PH &nbsp;</strong> </td>
       <td> <strong>Niveau d'eau</strong> </td>
   </tr>
   </thead>
   <tr>
       <td>&nbsp; 4/12/2019 &nbsp;</td>
       <td>  </td>
       <td>  </td>
       <td>  </td>
   </tr>
    <tr>
        <td> 5/12/2019 </td>
       <td>  </td>
       <td>  </td>
       <td>  </td>
   </tr>
   <tr>
        <td> 6/12/2019 </td>
       <td>  </td>
       <td>  </td>
       <td>  </td>
   </tr>
   <tr>
        <td> 7/12/2019 </td>
       <td>  </td>
       <td>  </td>
       <td>  </td>
   </tr> 
   <tr>
        <td> 8/12/2019 </td>
       <td>  </td>
       <td>  </td>
       <td>  </td>
   </tr> 
</table>
</article>
      </div>
  </section>


  </body>
</html>