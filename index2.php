<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mon Aquarium Connecté</title>
    <link rel="shortcut icon" type="image/png" href="image/favicon.ico"/>
    <link rel="stylesheet" href="css/ac.css">
  </head>
  <body>

    <!--Image haut de page -->
    <div class="img">
      <img src="image/affiche.png" alt="img" width="100%">
      <nav>
        <ul id="menu_horizontal">
          <img src="image/logo.jpg" class="logo" height="100%"></img>
          <li><a href="index.php">Aquarium</a></li>
          <li><a href="contact.php">Contact</a></li>
          <li><a href="historique.php">Historique</a></li>
          <li><a href="login.html">Se deconnecter</a></li>
        </ul>
      </nav>
    </div>
    <div id="content">
            <!-- tester si l'utilisateur est connecté -->
            <?php
                session_start();
                if($_SESSION['username'] !== ""){
                    $user = $_SESSION['username'];
                    // afficher un message
                    echo "Bonjour $user, vous êtes connecté";
                }
            ?>
            
    </div>
    <div class="grid-container">
    <br>
  <section>
       <FONT size="16"><strong><u> AQUARIUM </u></strong></FONT>
        <br>
        <br>

     
      <div id="alig">
    <article id="temp">
      <h1>Température</h1>
        <p>
            <br>
            <br>
            <FONT size="8"> 25°C </FONT>
        <br>
        <br>
        <br>
            Et licet quocumque oculos flexeris feminas adfatim multas
            spectare cirratas, quibus, si nupsissent, per aetatem ter iam
            nixus poterat suppetere liberorum, ad usque taedium pedibus
            pavimenta tergentes iactari volucriter gyris, dum exprimunt
            innumera simulacra, quae finxere fabulae theatrales.
            Et licet quocumque oculos flexeris feminas adfatim multas
            spectare cirratas, quibus, si nupsissent, per aetatem ter iam
            nixus poterat suppetere liberorum, ad usque taedium pedibus
            pavimenta tergentes iactari volucriter gyris, dum exprimunt
            innumera simulacra, quae finxere fabulae theatrales.
            Et licet quocumque oculos flexeris feminas adfatim multas
            spectare cirratas, quibus, si nupsissent, per aetatem ter iam
            nixus poterat suppetere liberorum, ad usque taedium pedibus
            pavimenta tergentes iactari volucriter gyris, dum exprimunt
          </p>
    </article>
      
     <article id="ph">
      <h1>Acidité</h1>
         <p>
             <br>
             <br>
             <FONT size="8"> 7 </FONT>
         
         <br>
         <br>
         <br>
            Et licet quocumque oculos flexeris feminas adfatim multas
            spectare cirratas, quibus, si nupsissent, per aetatem ter iam
            nixus poterat suppetere liberorum, ad usque taedium pedibus
            pavimenta tergentes iactari volucriter gyris, dum exprimunt
            innumera simulacra, quae finxere fabulae theatrales.
            Et licet quocumque oculos flexeris feminas adfatim multas
            spectare cirratas, quibus, si nupsissent, per aetatem ter iam
            nixus poterat suppetere liberorum, ad usque taedium pedibus
            pavimenta tergentes iactari volucriter gyris, dum exprimunt
            innumera simulacra, quae finxere fabulae theatrales.
            Et licet quocumque oculos flexeris feminas adfatim multas
            spectare cirratas, quibus, si nupsissent, per aetatem ter iam
            nixus poterat suppetere liberorum, ad usque taedium pedibus
            pavimenta tergentes iactari volucriter gyris, dum exprimunt
            innumera simulacra, quae finxere fabulae theatrales.
            Et licet quocumque oculos flexeris feminas adfatim multas
            spectare cirratas, quibus, si nupsissent, per aetatem ter iam
            nixus poterat suppetere liberorum, ad usque taedium pedibus
            pavimenta tergentes iactari volucriter gyris, dum exprimunt
            innumera simulacra, quae finxere fabulae theatrales.
          </p>
    </article>
      
      <article id="nv">
      <h1>Niveau d'eau</h1>
          <p>
              <br>
              <br>
              <FONT size="8"> 70% </FONT>
          <br>
          <br>
          <br>
            Et licet quocumque oculos flexeris feminas adfatim multas
            spectare cirratas, quibus, si nupsissent, per aetatem ter iam
            nixus poterat suppetere liberorum, ad usque taedium pedibus
            pavimenta tergentes iactari volucriter gyris, dum exprimunt
            innumera simulacra, quae finxere fabulae theatrales.
            Et licet quocumque oculos flexeris feminas adfatim multas
            spectare cirratas, quibus, si nupsissent, per aetatem ter iam
            nixus poterat suppetere liberorum, ad usque taedium pedibus
            pavimenta tergentes iactari volucriter gyris, dum exprimunt
            innumera simulacra, quae finxere fabulae theatrales.
            Et licet quocumque oculos flexeris feminas adfatim multas
            spectare cirratas, quibus, si nupsissent, per aetatem ter iam
            nixus poterat suppetere liberorum, ad usque taedium pedibus
            pavimenta tergentes iactari volucriter gyris, dum exprimunt
            innumera simulacra, quae finxere fabulae theatrales.
            Et licet quocumque oculos flexeris feminas adfatim multas
            spectare cirratas, quibus, si nupsissent, per aetatem ter iam
            nixus poterat suppetere liberorum, ad usque taedium pedibus
            pavimenta tergentes iactari volucriter gyris, dum exprimunt
            innumera simulacra, quae finxere fabulae theatrales.
          </p>
    </article>
      </div>  
  </section>


  </body>
</html>